
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const SignageOSPlugin = require('@signageos/cli/dist/Webpack/Plugin')
const webpack = require('webpack')

exports = module.exports = {
	entry: "./src/index.js",
	output: {
		filename: 'index.js',
	},
	resolve: {
		extensions: ['*', '.js', '.jsx'],
	},
	module: {
		rules: [
			{
				test: /^(.(?!.module.css))*.css$/,
				use: ['style-loader', 'css-loader'],
			},
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				options: { presets: [require.resolve('@babel/preset-env', '@babel/preset-react')] },
				enforce: 'post',
			},
			{
				test: /\.(png|jpg)$/,
				loader: 'url-loader'
			}
		],
	},
	plugins: [
			new HtmlWebpackPlugin({
				template: 'public/index.html',
				inlineSource: '.(js|css)$', // embed all javascript and css inline
			}),
			new HtmlWebpackInlineSourcePlugin(),
			new SignageOSPlugin(),
			new webpack.HotModuleReplacementPlugin()
	],
	devServer: {
		contentBase: './dist',
    hot: true
	}
};
