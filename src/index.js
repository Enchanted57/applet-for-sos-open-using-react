
require('./index.css');

import sos from '@signageos/front-applet';
import React from 'react';
import ReactDOM from 'react-dom';
import App from '../src/components/App'
// Wait on sos data are ready (https://docs.signageos.io/api/sos-applet-api/#onReady)
sos.onReady().then(async function () {
	const contentElement = document.getElementById('root');
	console.log('sOS is ready');
	ReactDOM.render(
		<App />,
		contentElement
	);
});

module.hot.accept();
