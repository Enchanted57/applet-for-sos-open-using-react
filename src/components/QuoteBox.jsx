import React from 'react';
import PropTypes from 'prop-types';

function QuoteBox({ quoteText, quoteNumber }) {

	return (
		<figure className="quote-box">
			<h2>Your daily Mr. Trump's quote batch: #{quoteNumber+1}</h2>
			<blockquote>
				{quoteText}
			</blockquote>
			<figcaption>
				-your friend Donald.
			</figcaption>
		</figure>
	);
}

QuoteBox.propTypes = {
	quoteText: PropTypes.string,
	quoteNumber: PropTypes.number
}

export default QuoteBox;
