import React, { useState, useEffect } from 'react';
import { data } from '../api/data';
import QuoteBox from './QuoteBox';

const updateInterval = 15e3;

function App() {
  const [index, setIndex] = useState(0);

  useEffect(() => {
    const updater = setInterval(() => {
      setIndex((index + 1) % data.length);
    }, updateInterval);

    return () => {
      clearInterval(updater);
    }
  });

  return (
    <div className="application">
      <QuoteBox quoteText={data[index]} quoteNumber={index} />
    </div>
  );
}

export default App;
